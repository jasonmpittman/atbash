/**
  * Atbash
  * https://github.com/jasonmpittman/atbash
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/
var alphabets = require('./alphabets.js');

module.exports = {
  encipher : function(input) {
    var ciphertext = new Array();
    var position;

    if (input === input.toUpperCase()) {
      var plaintext = input.toLowerCase().split("");
    } else {
      var plaintext = input.split("");
    }

    for (var i = 0; i < plaintext.length; i++) {
      position = alphabets.forward.indexOf(plaintext[i]);
      ciphertext.push(alphabets.forward[25 - position]);
    }

    return ciphertext.join("");
  },

  decipher : function(input) {
    var plaintext = new Array();
    var position;

    if (input === input.toUpperCase()) {
      var ciphertext = input.toLowerCase().split("");
    } else {
      var ciphertext = input.split("");
    }

    for (var i = 0; i < ciphertext.length; i++) {
      position = alphabets.forward.indexOf(ciphertext[i]);
      plaintext.push(alphabets.backward[0 + position]);
    }

    return plaintext.join("");
  }

};
