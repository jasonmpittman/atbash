/**
  * Atbash - decipher-test.js
  * https://github.com/jasonmpittman/atbash
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/
const atbash = require('./index.js');

var ciphertext = "KZHHDLIW";
var plaintext = atbash.decipher(ciphertext);

console.log(plaintext);
