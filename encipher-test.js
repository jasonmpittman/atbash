/**
  * Atbash - encipher-test.js
  * https://github.com/jasonmpittman/atbash
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/
const atbash = require('./index.js');

var plaintext = "password";
var ciphertext = atbash.encipher(plaintext);

console.log(ciphertext);
